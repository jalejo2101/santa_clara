    var anchoTotal =0;
    var alto = 0;
    var alturaMenu = 0;
    
    function altoSlider(){
        anchoTotal = $(document).width();
        if( anchoTotal <= 601)
            altoNew = 220;
        if( anchoTotal > 601 && anchoTotal < 993)
            altoNew = 400;
        if( anchoTotal >= 993)
            altoNew = 600;

        return altoNew;
    }

    function showMenuMap(){
        $(".menuMapa ul").slideToggle();
    }
    
    (function($) {          
        $(document).ready(function(){
            altura = $("#home").height();
            alturaMenu = $("#fixedMenu").height();
            showTab('panetones','selecto',1);
            showTab('panaderia','dulces',1);

            $('.slider').slider({full_width: true, indicators: false, height:altoSlider()});
            $(".button-collapse").sideNav({menuWidth: 240, closeOnClick: true });
            $("#panetonesList,#pasteleriaList, #novedadesList").tinycarousel({interval  : false});

            
            $(window).scroll(function(){
                var windscroll = $(window).scrollTop();
                if (windscroll > altura-100) {
                    $('#fixedMenu').fadeIn(500);
                    //$('#menu_mobile').css({"top":".6rem"});
                } else {
                    $('#fixedMenu').fadeOut(500);
                    //$('#menu_mobile').css({"top":"1rem"});
                } 
            });
        });
    })(jQuery);
    
    $(window).resize(function() {
        alto = altoSlider();
        $('.slides, .slider').css({"height":alto});
        alturaMenu = $("#fixedMenu").height();
    });
    

    $('a[href^="#"]').on('click', function(event) { 

        var target = $( $(this).attr('href') );
        if( target.length ) { 
            event.preventDefault();
            $('html, body').animate({
                scrollTop: (target.offset().top) - alturaMenu
            }, 700);
        }

    });

    function showTab(section, secToShow, n){
        $('#'+section+' .menuSlider li a').removeClass('activo');
        $('#'+section+' .menuSlider li:nth-child('+n+') a').addClass('activo');
        $('.'+section+'Type').fadeOut();
        $('.'+secToShow).fadeIn();
    }