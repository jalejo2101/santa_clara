    <section id="nosotros" class="minAlto">
       <div class="sectionSpaces container">
           <div class="row">
               <div class="col l8 m6 s12">
                   <h2>Nosotros</h2>
                   <?php echo $cont['nosotros_text']?>
               </div>
               <div class="center col l4 m6 s12">
                   <img class="responsive-img" src="img/contenido/<?php echo $cont['nosotros_img']?>">
               </div>
           </div>
       </div>
    </section>