    <section id="novedades" class="minAlto">
        <div class="sectionSpaces dashedLine container">
            <h2>Novedades</h2>
            
            <div class="row">
                <section class="catalogoSlider col s12">
                 <div id="novedadesList">
                      <a href="#" class="buttons prev"></a>
                      <div class="viewport">
                        <ul class="overview">
                            <?php $pnt=$con->get_novedades(); ?>

                            <?php foreach($pnt as $p){ ?>
                                <li>
                                    <header><div class="imgHead"><img src="img/novedades/<?php echo $p['img']?>"></div><h4><?php echo $p['titulo']?></h4></header>
                                    <p><?php echo $p['text']?></p>
                                </li>
                            <?php } ?>

                        </ul>
                      </div>
                      <a href="#" class="buttons next"></a>
                  </div>
                </section>
            </div>            
            
            
            
            <div class="row">
               <div class="col l7 m12 s12">
                   <p class="subtitulo">Responsabilidad social <span>SANTA CLARA</span> </p>
                   <img  class="responsive-img" src="img/contenido/<?php echo $cont['resp_social_img']?>">
                   <p><?php echo $cont['resp_social_text']?></p>
                   
               </div>
               <div class="center col l5 m12 s12">
                   <p class="subtitulo">¿Cómo hacer un verdadero Panetón?</p>
                   <div class="video-container">
                    <iframe width="420" height="315" src="<?php echo $cont['video_url']?>" frameborder="0" allowfullscreen></iframe>
                   </div>
               </div>
                
            </div>
        </div>
    </section>