    <section id="home">
            <div class="miniMenu hide-on-large-only"> <a href="#" data-activates="menuLateral" class="button-collapse"><img src="img/menuicon.png"></a> </div>
             <div id="top_menu_wrapper" class="hide-on-med-and-down">
                <div class="white_label">
                     <div class="container-large">
                      <div id="staticMenu">
                          <ul class="deskMenu left">
                            <li><a href="#nosotros">Nosotros</a></li>
                            <li><a href="#panetones">Panetones</a></li>
                            <li><a href="#panaderia">Panaderia y Pasteleria</a></li>
                            <li><a href="#ventas">Ventas</a></li>
                            <li><a href="#novedades">Novedades</a></li>
                            <li><a href="#tercerizacion">Tercerización</a></li>
                            <li><a href="#contacto">Contacto</a></li>
                          </ul>
                          
                          <ul class="socialMenu right">
                              <li>Síguenos en: </li>
                              <li><a href="#"> <img src="img/faceGreen.png"> </a> </li>
                          </ul>
                      </div>
                    </div>
                </div>
            </div>
            <?php
                $lst_bn=$con->get_lst_Banners_activos();
            ?>
            <div class="slider">
                <ul class="slides">
                    <?php foreach($lst_bn as $item){ ?>
                    <li>
                      <img src="img/banners/<?php echo $item['banner']?>">
                    </li>
                    <?php } ?>
                </ul>
            </div>             
    </section>