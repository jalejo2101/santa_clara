    <section id="ventas" class="minAlto">
        <div class="sectionSpaces dashedLine container">
            <h2>Ventas</h2>
            <div class="row">
               <div class="menuMapa col s12">
                   <a class="botonMap" onclick="showMenuMap()">DISTRIBUIDORES</a>
                   <!--ul>
                       <li> <a href="">LIMA</a> </li>
                       <li> <a href="#">PROVINCIAS</a> </li>
                   </ul-->
               </div>
               
               <div id="googleMap" style="height: 400px; width: 100%; margin: 30px 0">

               </div>
                <div class="col l6 m6 s12"> <a href="<?php echo $cont['ventas_url_1']?>"><img class="responsive-img" src="img/contenido/<?php echo $cont['ventas_img_1']?>"></a> </div>
                <div class="col l6 m6 s12"> <a href="<?php echo $cont['ventas_url_2']?>"><img class="responsive-img" src="img/contenido/<?php echo $cont['ventas_img_2']?>"></a> </div>
                <div class="col s12"> 
                    <h3>PUNTOS DE VENTA EN SUPERMERCADOS</h3>
                    <p><?php echo $cont['ventas_text']?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script>
        <?php
        $lst_suc=$con->get_sucursales_activas();
        $c=1;
        ?>
        <?php foreach($lst_suc as $item){
        echo  "var point".$c."=new google.maps.LatLng(".$item['latitud'].",".$item['longitud'].");";
        $c++;
        }?>


        function initialize()
        {
            var mapProp = {
                center:point1,
                zoom:13,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

            <?php for($i=1;$i<$c;$i++){
            echo "var marker".$i."=new google.maps.Marker({
                position:point".$i."
            });
            marker".$i.".setMap(map);";
            }?>
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
