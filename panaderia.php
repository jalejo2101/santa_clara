    <section id="panaderia" class="minAlto">
        <div class="sectionSpaces dashedLine container">
            <h2>Panaderia y Pasteleria</h2>
            
            <div class="row">
                <section class="col s12">
                    <ul class="menuSlider">
                        <li><a href="#"  class="activo"> <span class="hide-on-small-only">LINEA </span> DULCES </a></li>
                        <li><a href="#"> LINEA SECA </a></li>
                        <li><a href="#"> <span class="hide-on-small-only">LINEA </span> PASTELERÍA </a></li>
                    </ul>
                </section>
                <section class="catalogoSlider col s12">
                 <div id="pasteleriaList">
                      <a href="#" class="buttons prev"></a>
                      <div class="viewport">
                        <ul class="overview">
                            <?php $pnd=$con->get_panaderias_activas(); ?>


                            <?php foreach($pnd as $p){ ?>
                                <li>
                                    <header><div class="imgHead"><img src="img/panaderias/<?php echo $p['img']?>"></div><h4><?php echo $p['titulo']?></h4></header>
                                    <p><?php echo $p['text']?></p>
                                </li>
                            <?php } ?>



                           <!--li>
                               <header><div class="imgHead"><img src="img/nopic.png"></div><h4>Bizcocho Goyo x24</h4></header>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, </p>
                           </li>
                           <li>
                               <header><div class="imgHead"><img src="img/nopic.png"></div><h4>Bizcocho Navideño x24</h4></header>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, </p>
                           </li>
                           <li>
                               <header><div class="imgHead"><img src="img/nopic.png"></div><h4>Panques, pasas y frutas</h4></header>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, </p>
                           </li>
                           <li>
                               <header><div class="imgHead"><img src="img/nopic.png"></div><h4>Turrón</h4></header>
                               <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, </p>
                           </li-->
                        </ul>
                      </div>
                      <a href="#" class="buttons next"></a>
                  </div>
                </section>
            </div>
        </div>
    </section>