<?php
include_once("admin/includes/Consultas.php");
$con=new Consultas();
$cont=$con->get_contenido();
$cont=$cont[0]

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="favicon.ico">
	<link href="css/materialize.min.css" type="text/css" rel="stylesheet">
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>

    <link rel="shortcut icon" type="image/png" href="favicon.png"/>
    
	<title>Santa Clara</title>
</head>
<body>
  <div id="fixedMenu">
      <div class="container-large">
         <div class="hide-on-med-and-down">
              <ul class="deskMenu left">
                <li><a href="#nosotros">Nosotros</a></li>
                <li><a href="#panetones">Panetones</a></li>
                <li><a href="#panaderia">Panaderia y Pasteleria</a></li>
                <li><a href="#ventas">Ventas</a></li>
                <li><a href="#novedades">Novedades</a></li>
                <li><a href="#tercerizacion">Tercerización</a></li>
                <li><a href="#contacto">Contacto</a></li>
              </ul>

              <ul class="socialMenu right">
              	  <li id="LogoMenuDesk"><a href="#home"> <img src="img/contenido/<?php echo $cont['logo']?>"></a></li>
                  <li>Síguenos en: </li>
                  <li><a href="#"> <img src="img/faceWhite.png"> </a> </li>
              </ul>
          </div>
          
          <div class="logoMenu center hide-on-large-only">
              <a href="#" data-activates="menuLateral" class="menuMovil button-collapse"> <img src="img/menuicon.png"> </a>
              <a class="logoMain"> <img src="img/contenido/<?php echo $cont['logo']?>"> </a>
          </div>
      </div>
   </div>
   
   
   <ul class="side-nav" id="menuLateral" style="left: 0px;">
        <li><a href="#nosotros">Nosotros</a></li>
        <li><a href="#panetones">Panetones</a></li>
        <li><a href="#panaderia">Panaderia y Pasteleria</a></li>
        <li><a href="#ventas">Ventas</a></li>
        <li><a href="#novedades">Novedades</a></li>
        <li><a href="#tercerizacion">Tercerización</a></li>
        <li><a href="#contacto">Contacto</a></li>
   </ul>
   
   
   
   
    <?php include_once "home.php"; ?>
    <?php include_once "nosotros.php"; ?>
    <?php include_once "panetones.php"; ?>
    <?php include_once "panaderia.php"; ?>
    <?php include_once "ventas.php"; ?>
    <?php include_once "novedades.php"; ?>
    <?php include_once "tercerizacion.php"; ?>
    <?php include_once "contacto.php"; ?>



<script src="js/materialize.js"></script>
<script src="js/old.query.tinycarousel.js"></script>
<script src="js/init.js"></script>

</body>
</html>