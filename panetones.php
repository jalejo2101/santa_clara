    <section id="panetones" class="minAlto">
        <div class="sectionSpaces dashedLine container">
            <h2>Panetones</h2>
            <div class="row">
                <section class="col s12">
                    <ul class="menuSlider">
                        <li><a id="selecto" class="activo "> SELECTO </a></li>
                        <li><a id="tradicional" class=""> TRADICIONAL </a></li>
                        <li><a id="gaspare" class=""> GASPARE </a></li>
                    </ul>
                </section>
                <section class="catalogoSlider col s12">
                 <div id="panetonesList">
                      <a href="#" class="buttons prev"></a>
                      <div class="viewport" id="lista_pnt">
                        <ul class="overview" >
                           <?php $pnt=$con->get_panetones_activos(); ?>


                           <?php foreach($pnt as $p){ ?>
                           <li class="<?php echo $p['tipo']?>">
                               <header><div class="imgHead"><img src="img/panetones/<?php echo $p['img']?>"></div><h4><?php echo $p['titulo']?></h4></header>
                               <p><?php echo $p['text']?></p>
                           </li>
                            <?php } ?>
                        </ul>
                      </div>
                      <a href="#" class="buttons next"></a>
                  </div>
                </section>
            </div>
        </div>
    </section>

