<?php
session_start();

include_once("includes/Consultas.php");


$con=new Consultas();

if(isset($_GET['exit'])){
    if ($_GET['exit'] != null) {
        unset($_SESSION['user']);
        //unset($_SESSION['nombre']);
        //unset($_SESSION['mail']);
    }
}

if($_POST){
    //echo ">>>";
    $user=$_POST['user'];
    $password=md5($_POST['password']);
    $admin_usr=$con->validar_usuario($user,$password);
   // var_dump($admin_usr);
    if(count($admin_usr)==0 || $admin_usr==null){
        $err=true;
    }else{
        $_SESSION['user']=$admin_usr['user'];
        //$_SESSION['name']=$admin_usr['name'];
        //$_SESSION['email']=$admin_usr['email'];
        /* DIRECCION DE LA PAGINA INICIAL SI SE VALIDA EL USUARIO */
        //header('Location: banners.php');?>
	<script>window.location.href="banners.php"</script>
    <?php 
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>

<div class="row">
    <div class="col-md-6 col-md-offset-3 text-center">

        <div class="panel panel-default">
            <div class="panel-body">
                <p>
                    <img src="../img/logo.png" width="143">
                </p>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <form class="form-horizontal" role="form" method="POST" action="">
                            <div class="form-group">
                                <label for="user" class="col-xs-3 control-label text-left">Email</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="user" name="user" placeholder="User" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-xs-3 control-label text-left">Password</label>
                                <div class="col-xs-9">
                                    <input type="password" class="form-control" name="password" id="password"  placeholder="Password" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-7 text-center">
                                    <button type="submit" class="btn btn-default" name="btn">Entrar</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div id="alerta" class="alert alert-danger text-center" style="display:<?php echo (!$err)?"none":"block" ?>">El Usuario NO Existe</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script>

</script>

</html>