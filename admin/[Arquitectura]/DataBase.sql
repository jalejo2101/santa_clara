-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-01-2016 a las 02:28:10
-- Versión del servidor: 10.0.17-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `santa_clara`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `mail`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@santaclara.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `banner` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `banner`, `activo`) VALUES
(1, 'banner01_9051.png', 1),
(2, 'banner02_8830.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE `contenido` (
  `id` int(11) NOT NULL,
  `nosotros_text` text,
  `nosotros_img` varchar(32) DEFAULT NULL,
  `ventas1` varchar(32) DEFAULT NULL,
  `ventas2` varchar(32) DEFAULT NULL,
  `ventas_text` varchar(256) DEFAULT NULL,
  `resp_social_text` text,
  `resp_social_img` varchar(32) DEFAULT NULL,
  `video_url` varchar(120) DEFAULT NULL,
  `tercerizacion_text` text,
  `tercerizacion_img` varchar(32) DEFAULT NULL,
  `destino_correo` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`id`, `nosotros_text`, `nosotros_img`, `ventas1`, `ventas2`, `ventas_text`, `resp_social_text`, `resp_social_img`, `video_url`, `tercerizacion_text`, `tercerizacion_img`, `destino_correo`) VALUES
(1, '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda</div>', 'paneton01_4847.png', NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur a', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo.', 'responsabilidad_6345.png', 'https://www.youtube.com/watch?v=AgnjWj793gE', '<div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda</div><div><br></div><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda<span style="font-size: 14px; letter-spacing: normal; line-height: 1.42857; background-color: rgb(255, 255, 255);">&nbsp;</span><span style="font-size: 14px; letter-spacing: normal; line-height: 1.42857; background-color: rgb(255, 255, 255);">assumenda</span><span style="font-size: 14px; letter-spacing: normal; line-height: 1.42857; background-color: rgb(255, 255, 255);">&nbsp;</span><span style="font-size: 14px; letter-spacing: normal; line-height: 1.42857; background-color: rgb(255, 255, 255);">assumenda</span></div>', 'tercerizacion_8950.png', 'jalejo@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE `novedades` (
  `id` int(11) NOT NULL,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'PanetÃ³n SACHA INCHI', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_7205.png', '', 1),
(2, 'Loncherita SANTA CLARA', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_7971.png', '', 1),
(3, 'Ponle su Nombre', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_6191.png', '', 1),
(4, 'Otro Ejemplo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_9997.png', '', 1),
(5, 'SANTA CLARA', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_8643.png', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panaderia`
--

CREATE TABLE `panaderia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `panaderia`
--

INSERT INTO `panaderia` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'Bizcocho Goyo x24', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda', 'nopic_1993.png', '', 1),
(2, 'Bizcocho NavideÃ±o x24', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam', 'nopic_1728.png', '', 1),
(3, 'Panques, pasas y frutas', 'zxc zxc zxc', 'nopic_5104.png', '', 1),
(4, 'Turron', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'nopic_3210.png', '', 1),
(5, 'PanetÃ³n SACHA INCHI', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_6298.png', '', 1),
(6, 'PanetÃ³n SACHA INCHI', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_4439.png', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panetones`
--

CREATE TABLE `panetones` (
  `id` int(11) NOT NULL,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `panetones`
--

INSERT INTO `panetones` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'Selecto Bolsa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'paneton01_9399.png', 'SELECTO', 1),
(2, 'Selecto Chispas de Chocolate', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto02_9514.png', 'SELECTO', 1),
(3, 'Selecto Premium', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto01_7469.png', 'TRADICIONAL', 1),
(5, 'Selecto Caja', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto02_2292.png', 'TRADICIONAL', 1),
(6, 'Selecto', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto02_8611.png', 'GASPARE', 1),
(7, 'Bolsa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto02_2847.png', 'TRADICIONAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL,
  `sucursal` varchar(64) DEFAULT NULL,
  `latitud` varchar(32) DEFAULT NULL,
  `longitud` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `sucursal`, `latitud`, `longitud`, `activo`) VALUES
(1, 'Principal', '-12.055342', '-77.0802049', 1),
(2, 'Segunda', '-12.05832', '-77.080303', 1),
(3, 'Tercera', '-12.0587', '-77.0806', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contenido`
--
ALTER TABLE `contenido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `novedades`
--
ALTER TABLE `novedades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `panaderia`
--
ALTER TABLE `panaderia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `panetones`
--
ALTER TABLE `panetones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `contenido`
--
ALTER TABLE `contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `novedades`
--
ALTER TABLE `novedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `panaderia`
--
ALTER TABLE `panaderia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `panetones`
--
ALTER TABLE `panetones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
