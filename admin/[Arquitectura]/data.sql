-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 14-01-2016 a las 05:47:52
-- Versión del servidor: 5.5.45
-- Versión de PHP: 5.4.44

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `data`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `password`, `mail`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'admin@santaclara.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `banner`, `activo`) VALUES
(1, 'banner01_9051.png', 1),
(2, 'banner02_8830.png', 1),
(3, 'banner_1639661.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenido`
--

CREATE TABLE IF NOT EXISTS `contenido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nosotros_text` text,
  `nosotros_img` varchar(32) DEFAULT NULL,
  `ventas_img_1` varchar(32) DEFAULT NULL,
  `ventas_url_1` varchar(32) DEFAULT NULL,
  `ventas_img_2` varchar(32) DEFAULT NULL,
  `ventas_url_2` varchar(32) DEFAULT NULL,
  `ventas_text` varchar(256) DEFAULT NULL,
  `resp_social_text` text,
  `resp_social_img` varchar(32) DEFAULT NULL,
  `video_url` varchar(120) DEFAULT NULL,
  `tercerizacion_text` text,
  `tercerizacion_img` varchar(32) DEFAULT NULL,
  `destino_correo` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `contenido`
--

INSERT INTO `contenido` (`id`, `nosotros_text`, `nosotros_img`, `ventas_img_1`, `ventas_url_1`, `ventas_img_2`, `ventas_url_2`, `ventas_text`, `resp_social_text`, `resp_social_img`, `video_url`, `tercerizacion_text`, `tercerizacion_img`, `destino_correo`) VALUES
(1, '<div>Bienvenido a nuestra pÃ¡gina web, somos Santa Clara, una empresa familiar que desde hace mÃ¡s de 20 aÃ±os forma parte de la mesa de cientos de hogares peruanos. Nos dedicamos a la elaboraciÃ³n de productos de panaderÃ­a de sabores apetitosos. Nos distinguimos por el trato personalizado que brindamos a nuestros clientes (amas de casa y pÃºblico en general).</div><div><br></div><div> Si desea probar los mÃ¡s deliciosos panetones y turrones, entonces somos su alternativa perfecta. Solo ofrecemos productos de primera calidad, los cuales son distribuidos en los supermercados de Plaza Vea. Gracias a la preferencia del pÃºblico obtuvimos en julio de 2009 el premio Big Business Latin American Businessmen.</div>', 'paneton01_4847.png', 'ventas_img_1_2152874.', 'www.tottus.com', 'ventas_img_2_4108159.', 'www.plazavea.com.pe', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda, quae ab dolorum ullam aliquam officia temporibus id voluptatem explicabo?Lorem ipsum dolor sit amet, consectetur a', 'Como empresa peruana, estamos conscientes de que no estamos solos sino que somos parte de una gran comunidad. Es por ello, que aÃ±o a aÃ±o somos parte de campaÃ±as que suman a los nuestros.', 'responsabilidad_6345.png', 'https://www.youtube.com/embed/AgnjWj793gE', '<div>Grandes, medianas y pequeÃ±as compaÃ±Ã­as nos dan su confianza para sus marcas en nuestros productos, como panetones, turrones, etc. Hemos podido trabajar con distintas compaÃ±Ã­as, entre las que destacan Bells y el club de fÃºtbol peruano, Alianza Lima.</div><div><br></div><div>ConfÃ­a en nuestra vasta experiencia y pÃ³ngale su nombre a nuestros panetones o a cualquier producto que encuentre en nuestra pÃ¡gina web.</div>', 'tercerizacion_8950.png', 'alexoriarodriguez@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `novedades`
--

CREATE TABLE IF NOT EXISTS `novedades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `novedades`
--

INSERT INTO `novedades` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'PanetÃ³n SACHA INCHI', 'Busca alcanzar el primer lugar como producto navideÃ±o saludable y oriundo, fuente de Ã¡cidos grasos esenciales para la vida del ser humano. Contiene Omega 3, 6, 9, proteÃ­nas y antioxidantes.', 'nopic_7205.png', '', 1),
(2, 'Loncherita SANTA CLARA', 'Para los engreÃ­dos de la casa, Santa Clara ha lanzado la loncherita que contiene nutritivos productos: milhojas, galleta, quequito, para que cada dÃ­a de la semana nuestros niÃ±os tengan un rico y nutritivo bocadito en su lonchera.', 'nopic_7971.png', '', 1),
(3, 'PÃ³nle su Nombre', 'ConfÃ­a en nuestra vasta experiencia y pÃ³ngale su nombre a nuestros panetones o a cualquier producto que encuentre en nuestra pÃ¡gina web', 'nopic_6191.png', '', 1),
(4, 'Otro Ejemplo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'novedades_3947195.png', '', 1),
(5, 'SANTA CLARA', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,', 'nopic_8643.png', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panaderia`
--

CREATE TABLE IF NOT EXISTS `panaderia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `panaderia`
--

INSERT INTO `panaderia` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'Bizcocho Goyo x24', 'Delicioso bizcocho hecho a base de la masa del panetÃ³n.', 'selecto04_9781.png', '', 1),
(2, 'Bizcocho NavideÃ±o x24', 'Exquisito bizcocho hecho a base de la masa del panetÃ³n, hecho especialmente para estas fiestas especiales.', 'panaderia_1009561.png', '', 1),
(3, 'Panques, pasas y frutas', 'zxc zxc zxc', 'panaderia_2100340.png', '', 1),
(4, 'Turron', 'Selectos palitos baÃ±ados en miel 100% de fruta natural, a diferencia de la competencia utilizamos esta miel que hace a nuestro turrÃ³n diferente, delicioso y selecto. PresentaciÃ³n por un kilo, medio kilo y 250 gramos.<br>', 'panaderia_1767144.png', '', 1),
(5, 'PanetÃ³n SACHA INCHI', 'Busca alcanzar el primer lugar como producto navideÃ±o saludable y oriundo, fuente de Ã¡cidos grasos esenciales para la vida del ser humano. Contiene Omega 3, 6, 9, proteÃ­nas y antioxidantes.', 'nopic_6298.png', '', 1),
(6, 'Panetoncito x 90 gr ...', 'Selecto  de la misma calidad  en presentaciÃ³n curiosa para el niÃ±o de la casa. ....', 'panaderia_6654933.png', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `panetones`
--

CREATE TABLE IF NOT EXISTS `panetones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(32) DEFAULT NULL,
  `text` varchar(256) DEFAULT NULL,
  `img` varchar(32) DEFAULT NULL,
  `tipo` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `panetones`
--

INSERT INTO `panetones` (`id`, `titulo`, `text`, `img`, `tipo`, `activo`) VALUES
(1, 'Selecto Bolsa', 'PanetÃ³n en una presentaciÃ³n familiar. Es obtenido por el amasamiento y cocciÃ³n de masa debidamente desarrollada por el proceso de fermentaciÃ³n, hecho de harina de trigo especial.<br>', 'paneton01_9399.png', 'SELECTO', 1),
(2, 'Selecto Chispas de Chocolate', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'paneton_8556713.png', 'SELECTO', 1),
(3, 'Selecto Premium', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'selecto01_7469.png', 'TRADICIONAL', 1),
(5, 'Selecto Caja', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia suscipit quia nulla eaque eos magnam enim tempora vitae laborum assumenda,<br>', 'paneton_6570674.png', 'TRADICIONAL', 1),
(6, 'Selecto', 'Nuestro PanetÃ³n Gaspare elaborado bajo los mismos estÃ¡ndares de calidad, es un panetÃ³n econÃ³mico con la misma calidad que garantiza Santa Clara.<br>', 'paneton_1114720.png', 'GASPARE', 1),
(7, 'Bolsa', 'Nuestro PanetÃ³n Tradicional elaborado con una receta diferente emana sabor y calidad excepcional, con la garantÃ­a de la marca Santa Clara.<br>', 'selecto02_2847.png', 'TRADICIONAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE IF NOT EXISTS `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sucursal` varchar(64) DEFAULT NULL,
  `latitud` varchar(32) DEFAULT NULL,
  `longitud` varchar(32) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `sucursal`, `latitud`, `longitud`, `activo`) VALUES
(1, 'Principal', '-12.055342', '-77.0802049', 1),
(2, 'Segunda', '-12.05832', '-77.080303', 1),
(3, 'Tercera', '-12.0587', '-77.0806', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
