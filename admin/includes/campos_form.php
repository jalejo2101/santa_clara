<script>
    function check(input) {
        if (input.value == "") {
            input.setCustomValidity('Debe completar este campo.');
        } else  {
            input.setCustomValidity('');
        }
    }
</script>

<?php
$wl=3;
$wi=9;

function input($id,$label,$tipo="text"){
    global $wl, $wi;
    if($tipo!="file")
        $clase='class="form-control"';
    echo '
    <div class="form-group">
        <label for="'.$id.'" class="col-xs-'.$wl.'  control-label">'.$label.'</label>
            <div class="col-xs-'.$wi.' ">
                <input type="'.$tipo.'" '.$clase.' id="'.$id.'" name="'.$id.'" placeholder="'.$label.'">
        </div>
    </div>';
}

function input_req($id,$label,$tipo="text"){
    global $wl, $wi;
    if($tipo!="file")
        $clase='class="form-control"';
    echo '
    <div class="form-group">
        <label for="'.$id.'" class="col-xs-'.$wl.'  control-label">'.$label.'</label>
            <div class="col-xs-'.$wi.' ">
                <input type="'.$tipo.'" '.$clase.' id="'.$id.'" name="'.$id.'" placeholder="'.$label.'">
        </div>
    </div>';
}
function input_value($id,$label,$valor,$tipo="text"){
    global $wl, $wi;
    if($tipo!="file")
        $clase='class="form-control"';
    echo '
    <div class="form-group">
        <label for="'.$id.'" class="col-xs-'.$wl.'  control-label">'.$label.'</label>
            <div class="col-xs-'.$wi.' ">
                <input type="'.$tipo.'" '.$clase.' id="'.$id.'" name="'.$id.'" placeholder="'.$label.'" value="'.$valor.'" >
        </div>
    </div>';
}


function input_vertical($id,$label,$tipo="text"){
    global $wl, $wi;
    echo '
        <div>
        <label>'.$label.'</label>
                <input type="'.$tipo.'" id="'.$id.'" name="'.$id.'" placeholder="'.$label.'" >
        </div>';
}

function input_vertical_req($id,$label,$msg,$tipo="text"){
    global $wl, $wi;
    echo '<div>
        <label>'.$label.'</label>
                <input class="form-control" type="'.$tipo.'" id="'.$id.'" name="'.$id.'" placeholder="'.$label.'" required oninvalid="check(this,\''.$msg.'\')">
        </div>';
}

function area_texto_label($id,$label){
    global $wl, $wi;
    echo '
        <div class="form-group">
            <label for="'.$id.'" class="col-xs-'.$wl.'  control-label">'.$label.'</label>
            <div class="col-xs-'.$wi.' ">
                <textarea class="form-control" rows="3" id="'.$id.'" name="'.$id.'"></textarea>
            </div>
        </div>';
}

function area_texto($id,$rows){
    global $wl, $wi;
    echo '
        <div class="form-group">
            <textarea class="form-control" rows="'.$rows.'" id="'.$id.'" name="'.$id.'"></textarea>
        </div>';
}

function area_texto_contenido($id,$rows,$contenido){
    global $wl, $wi;
    echo '
        <div class="form-group">
            <textarea class="form-control" rows="'.$rows.'" id="'.$id.'" name="'.$id.'">'.$contenido.'</textarea>
        </div>';
}




function combobox($id,$label,$text_list,$val_list=1){
    global $wl, $wi;
    if($val_list==1)
        $val_list=$text_list;
    echo '
        <div class="form-group">
            <label for="'.$id.'" class="col-xs-'.$wl.'  control-label">'.$label.'</label>
            <div class="col-xs-'.$wi.' ">
                <select class="form-control" id="'.$id.'" name="'.$id.'">
                    ';
                    for($i=0; $i<count($text_list); $i++){
                        echo '<option value="'.$val_list[$i].'">'.$text_list[$i].'</option>';
                    }
    echo '      </select>
            </div>
        </div>';
}

function boton($id,$text,$tipo,$onclick=1){
    $onclick=($onclick==1)?"":"onclick='".$onclick."'";
    echo '
            <div class="form-group">
                <div class="col-xs-offset-4 col-xs-4 text-center">
                    <button id="'.$id.'" type="'.$tipo.'" class="btn btn-default" '.$onclick.' >'.$text.'</button>
                </div>
            </div>';
}
?>