<?php

include_once("Consultas_ja.php");

/********************************************************************************************************************/

/********************************************************************************************************************/

/********************************************************************************************************************/




function agregar_item_banner(){
    $con=new Consultas();
    $tipo_item=$_POST["tipo_item"];
    $imagen=$_FILES["imagen"]["name"];
    $ft=$_FILES["imagen"]["type"];
    $img_existe=true;

    if($_POST){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["imagen"]["error"] > 0)
            {
                if (!file_exists("img/premios/".$_FILES["imagen"]["name"])) {
                    move_uploaded_file($_FILES["imagen"]["tmp_name"],"../imgs/banners/".$_FILES["imagen"]["name"]);
                    $img_existe=false;
                }
            }
            if(!$img_existe){
                $sql="UPDATE banner SET ".$tipo_item."='$imagen' WHERE id='100'";
                //echo $sql;
                $con->grabar($sql);
                $msg='Imagen Agregada!';
            }else{
                $msg='El Archivo de imagen ya existe!';
            }
        }

        if($msg!=null){
            echo "
            <script>

            alert('".$msg."');

            //window.history.back();
            </script>";
        }
    }
}


function agregar_contenido(){

    $con=new Consultas();

    $seccion  =$_POST["seccion"];
    $contenido=$_POST["contenido"];

    $sql="UPDATE contenidos SET ".$seccion."='".$contenido."' WHERE `id`='1000'";

    //echo $sql;
    $con->grabar($sql);

}


function agrega_equipo(){
    $con=new Consultas();
    
    $randnum = rand(1000, 99000);
    
    $nombre   =$_POST["nombre"];
    $cargo=$_POST["cargo"];
    $descripcion =$_POST["descripcion"];
    
    $iName = substr($_FILES["foto"]["name"], 0, -4); //Nombre de la imagen
    $iExt = substr($_FILES["foto"]["name"], -4); // Extencion de la imagen con el .

    $imagen = $iName."-".$randnum."$iExt"; // Nuevo nombre de imagen con random para que el usuario pueda colocar imagenes con el mismo nombre

    $ft=$_FILES["foto"]["type"];

    $allsize = getimagesize($_FILES["foto"]["tmp_name"]);
    $anchoFoto = $allsize[0];
    $altoFoto = $allsize[1];
    
    if ($imagen=="" || $ft==""){
            $imagen="noface.png";
            $ft="image/png";
            $anchoFoto = 166;
            $altoFoto = 166;
    }

    $img_existe=true;
        
    if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png" && $anchoFoto == $altoFoto ))
    {
        if (!$_FILES["foto"]["error"] > 0)
        {
            if (!file_exists("../imgs/team/" . $imagen)) {
                move_uploaded_file($_FILES["foto"]["tmp_name"],"../imgs/team/" . $imagen);
                $img_existe=false;
            }
        }

        if(!$img_existe || $imagen=="noface.png"){
            $sql="INSERT INTO equipo (nombre,cargo,descripcion, foto) VALUES ('$nombre','$cargo','$descripcion','$imagen')";
            header('Location: equipo.php?res=ok');
        }else{
            $msg='El Archivo de imagen ya existe!';
            header('Location: equipo.php?err=2');
        }
    }
    else
        header('Location: equipo.php?err=1'); //echo "<script> alert('La imagen debe ser cuadrada');</script>";
    
    
    //echo $sql;
    $con->grabar($sql);
    header('Location: equipo.php');
}

function edita_equipo(){
    $con=new Consultas();
    
    $randnum = rand(1000, 99000);

    $id = $_POST["id"];
    $nombre = $_POST["nombre"];
    $cargo=$_POST["cargo"];
    $descripcion =$_POST["descripcion"];
    
    $iName = substr($_FILES["foto"]["name"], 0, -4); //Nombre de la imagen
    $iExt = substr($_FILES["foto"]["name"], -4); // Extencion de la imagen con el .

    $imagen = $iName."-".$randnum."$iExt"; // Nuevo nombre de imagen con random para que el usuario pueda colocar imagenes con el mismo nombre

    $ft=$_FILES["foto"]["type"];

    $allsize = getimagesize($_FILES["foto"]["tmp_name"]);
    $anchoFoto = $allsize[0];
    $altoFoto = $allsize[1];
    

    if ($imagen=="" || $ft==""){
        $sql="UPDATE equipo SET nombre = '$nombre', cargo = '$cargo', descripcion = '$descripcion' WHERE id = $id";
        header('Location: equipo.php'); 
    }
    else if ($anchoFoto == $altoFoto){
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["foto"]["error"] > 0)
            {
                if (!file_exists("../imgs/team/" . $imagen)) {
                    move_uploaded_file($_FILES["foto"]["tmp_name"],"../imgs/team/" . $imagen);
                    
                    $sql="UPDATE equipo SET nombre = '$nombre', cargo = '$cargo', descripcion = '$descripcion', foto = '$imagen' WHERE id = $id";
                    header('Location: equipo.php'); 
                }
            }
            else{
                $msg='El Archivo de imagen ya existe!';
                header('Location: equipo.php?err=1'); 
            }
        }
        else header('Location: equipo.php?err=2'); 
   }
    
    
    
    $con->grabar($sql);
    header('Location: equipo.php');
}


function agrega_noticia(){
    $con=new Consultas();
    
    $randnum = rand(1000, 99000);
    
    $titulo   =$_POST["titulo"];
    $descripcion =$_POST["descripcion"];
    
    $iName = substr($_FILES["foto"]["name"], 0, -4); //Nombre de la imagen
    $iExt = substr($_FILES["foto"]["name"], -4); // Extencion de la imagen con el .

    $imagen = $iName."-".$randnum."$iExt"; // Nuevo nombre de imagen con random para que el usuario pueda colocar imagenes con el mismo nombre

    $ft=$_FILES["foto"]["type"];
    
    if ($imagen=="" || $ft==""){
            $imagen="noimagen.png";
            $ft="image/png";
    }

    $img_existe=true;
        
    if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png" ))
    {
        if (!$_FILES["foto"]["error"] > 0)
        {
            if (!file_exists("../imgs/noti/" . $imagen)) {
                move_uploaded_file($_FILES["foto"]["tmp_name"],"../imgs/noti/" . $imagen);
                $img_existe=false;
            }
        }

        if(!$img_existe || $imagen=="noimagen.png"){
            $sql="INSERT INTO noticias (titulo, descripcion, foto, fecha) VALUES ('$titulo','$descripcion','$imagen', NOW())";
            header('Location: noticias.php?res=ok');
        }else{
            $msg='El Archivo de imagen ya existe!';
            header('Location: noticias.php?err=2');
        }
    }
    else
        header('Location: noticias.php?err=1'); //echo "<script> alert('La imagen debe ser cuadrada');</script>";
    
    
    //echo $sql;
    $con->grabar($sql);
    header('Location: noticias.php');
}

function edita_noticia(){
    $con=new Consultas();
    
    $randnum = rand(1000, 99000);

    $id = $_POST["id"];
    $titulo = $_POST["titulo"];
    $fecha = $_POST["fecha"];
    $descripcion =$_POST["descripcion"];
    
    $iName = substr($_FILES["foto"]["name"], 0, -4); //Nombre de la imagen
    $iExt = substr($_FILES["foto"]["name"], -4); // Extencion de la imagen con el .

    $imagen = $iName."-".$randnum."$iExt"; // Nuevo nombre de imagen con random para que el usuario pueda colocar imagenes con el mismo nombre

    $ft=$_FILES["foto"]["type"];

    

    if ($imagen=="" || $ft==""){
        $sql="UPDATE noticias SET titulo = '$titulo', fecha = '$fecha',descripcion = '$descripcion' WHERE id = $id";
        header('Location: noticias.php'); 
    }
    else{
        if (($ft == "image/gif" || $ft == "image/jpeg" || $ft == "image/jpg" || $ft == "image/png"))
        {
            if (!$_FILES["foto"]["error"] > 0)
            {
                if (!file_exists("../imgs/noti/" . $imagen)) {
                    move_uploaded_file($_FILES["foto"]["tmp_name"],"../imgs/noti/" . $imagen);
                    
                    $sql="UPDATE noticias SET titulo = '$titulo', fecha = '$fecha', descripcion = '$descripcion', foto = '$imagen' WHERE id = $id";
                    header('Location: noticias.php'); 
                }
            }
            else{
                $msg='El Archivo de imagen ya existe!';
                header('Location: noticias.php?err=1'); 
            }
        }
        else header('Location: noticias.php?err=2'); 
   }
    
    
    
    $con->grabar($sql);
    header('Location: noticias.php');
}


function borra_equipo($id){
    $con=new Consultas();

    $sql="DELETE FROM equipo WHERE id='$id'";
    $con->grabar($sql);
}

function borra_noticia($id){
    $con=new Consultas();

    $sql="DELETE FROM noticias WHERE id='$id'";
    $con->grabar($sql);
}
/*****************************************************************************************************/
/*****************************************************************************************************/
/*****************************************************************************************************
function agrega_local(){
    $con=new Consultas();

    $nombre   =$_POST["nombre"];
    $localidad=$_POST["localidad"];
    $contacto =$_POST["contacto"];
    $telefono =$_POST["telefono"];
    $fecha    =date("Y-m-d");

    $sql="INSERT INTO locales
          (nombre,localidad,contacto,telefono,fecha_alta)
          VALUES
          ('$nombre','$localidad','$contacto','$telefono','$fecha')";
    //echo $sql;
    $con->grabar($sql);
}


function agrega_promocion(){
    $con=new Consultas();

    $id_local  =$_POST["id_local"];
    $tipo_url  =$_POST["tipo_url"];
    $nombre_url=$_POST["nombre_url"];
    $texto_mail=$_POST["texto_mail"];
    $terminos  =$_POST["terminos"];

    $sql="INSERT INTO promociones
          (id_local,tipo_url,nombre_url,texto_mail,terminos)
          VALUES
          ('$id_local','$tipo_url','$nombre_url','$texto_mail','$terminos')";
    //echo $sql;
    $con->grabar($sql);
}



function eliminar_de_tabla($tabla){
    $con=new Consultas();
    $id=$_POST["id"];
    $sql="DELETE FROM $tabla WHERE id='$id'";
    $con->grabar($sql);
}*/

?>