<?php
/**
 * Created by Juan Alejo.
 * User: juan
 * Date: 20/3/15
 * Time: 12:05 PM
 */
error_reporting(0);
require_once('DBManager.php');

class Consultas extends DBManager
{

    var $con;

    function  Consultas()
    {
        $this->con = new DBManager;
    }


    function grabar($sql)
    {
        if ($this->con->conectar() == true) {
            mysql_query($sql);
        }
    }

    /*************************************************
     * Ejecucion de consulta Generica
     *************************************************/
    public function consulta_sql($sql)
    {
        $result = null;
        if ($this->con->conectar() == true) {
            $result = mysql_query($sql);
            return $result;
        }
    }

    /*************************************************
     * Retorna los datos del usuario segun su nombre y
     * password dentro de la BD
     ************************************************/
    public function validar_usuario($user, $pass)
    {
        $sql = "SELECT * FROM admin WHERE user='$user' AND password='$pass'";
        $res = $this->consulta_sql($sql);
        $lista = array();
        //echo $sql;
        while ($row = mysql_fetch_assoc($res)) {
            $lista[] = $row;
        }
        return $lista[0];
    }

    /***********************************/
    /*lista de imagenes Banner         */
    /***********************************/
    function get_lst_Banners()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM banners");
            while ($row = mysql_fetch_assoc($result)) {
                $this->listab[] = $row;
            }
            return $this->listab;
        }
    }
    /************************************/
    /*lista de imagenes Banners activos */
    /************************************/
    function get_lst_Banners_activos()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM banners where activo=1");
            while ($row = mysql_fetch_assoc($result)) {
                $this->listab[] = $row;
            }
            return $this->listab;
        }
    }


    /***********************************/
    /* Banner por ID                   */
    /***********************************/
    function get_banner_id($id)
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM banners where id=$id");
            while ($row = mysql_fetch_assoc($result)) {
                $this->banner[] = $row;
            }
            return $this->banner[0];
        }
    }

    /***********************************/
    /* Listado de Panaderias           */
    /***********************************/
    function get_panaderias()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panaderia");
            while ($row = mysql_fetch_assoc($result)) {
                $this->panaderias[] = $row;
            }
            return $this->panaderias;
        }
    }
    /***********************************/
    /* Listado de Panaderias activas   */
    /***********************************/
    function get_panaderias_activas()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panaderia WHERE activo=1");
            while ($row = mysql_fetch_assoc($result)) {
                $this->panaderias[] = $row;
            }
            return $this->panaderias;
        }
    }



    /***********************************/
    /* Datos de Panaderia por ID       */
    /***********************************/
    function get_panaderia($id)
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panaderia WHERE id=$id");
            $row = mysql_fetch_assoc($result);
            return $row;
        }
    }


    /***********************************/
    /* Listado de Panetones           */
    /***********************************/
    function get_panetones()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panetones");
            while ($row = mysql_fetch_assoc($result)) {
                $this->panetones[] = $row;
            }
            return $this->panetones;
        }
    }

    /***********************************/
    /* Listado de Panetones activos   */
    /***********************************/
    function get_panetones_activos()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panetones WHERE activo=1");
            while ($row = mysql_fetch_assoc($result)) {
                $this->panetones[] = $row;
            }
            return $this->panetones;
        }
    }

    /***********************************/
    /* Datos de Panetones por ID       */
    /***********************************/
    function get_paneton($id)
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM panetones WHERE id=$id");
            $row = mysql_fetch_assoc($result);
            return $row;
        }
    }



    /***********************************/
    /* Contenido                       */
    /***********************************/
    function get_contenido()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM contenido WHERE id='1'");
            while ($row = mysql_fetch_assoc($result)) {
                $this->contenido[] = $row;
            }
            return $this->contenido;
        }
    }

    /***********************************/
    /* Datos de Novedad por ID       */
    /***********************************/
    function get_novedad($id)
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM novedades WHERE id=$id");
            $row = mysql_fetch_assoc($result);
            return $row;
        }
    }


    /***********************************/
    /* Listado de Nocedades           */
    /***********************************/
    function get_novedades()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM novedades");
            while ($row = mysql_fetch_assoc($result)) {
                $this->novedades[] = $row;
            }
            return $this->novedades;
        }
    }
    /***********************************/
    /* Listado de Novedades activas   */
    /***********************************/
    function get_novedades_activas()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM novedades WHERE activo=1");
            while ($row = mysql_fetch_assoc($result)) {
                $this->novedades[] = $row;
            }
            return $this->novedades;
        }
    }




    /***********************************/
    /* Datos de Sucursal por ID       */
    /***********************************/
    function get_sucursal($id)
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM sucursales WHERE id=$id");
            $row = mysql_fetch_assoc($result);
            return $row;
        }
    }


    /***********************************/
    /* Listado de Sucursales           */
    /***********************************/
    function get_sucursales()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM sucursales");
            while ($row = mysql_fetch_assoc($result)) {
                $this->sucursales[] = $row;
            }
            return $this->sucursales;
        }
    }
    /***********************************/
    /* Listado de Sucursales activas   */
    /***********************************/
    function get_sucursales_activas()
    {
        if ($this->con->conectar() == true) {
            $result = mysql_query("SELECT * FROM sucursales WHERE activo=1");
            while ($row = mysql_fetch_assoc($result)) {
                $this->sucursales[] = $row;
            }
            return $this->sucursales;
        }
    }
    /**************************************************************************************************/
    /**************************************************************************************************/
    /**************************************************************************************************/

}


?>