<?php include_once("includes/Consultas.php");?>
<?php include_once("includes/funciones.php");?>
<?php

if($_POST["modo"]=="new"){
    agrega_sucursal();
}else if($_POST["modo"]=="update"){
    modifica_sucursal();
}if($_POST["modo"]=="delete"){
    elimina_sucursal();
}
$con=new Consultas();
$lst=$con->get_sucursales();
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar esta Sucursal?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Sucursales</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('sucursal.php','_self','')">Agregar</button>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=14 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td>Sucursal</td>
                <td style="width: 20%; text-align: center">Latitud</td>
                <td style="width: 20%; text-align: center">Longitud</td>
                <td style="width: 10%; text-align: center">Activo</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['sucursal']?></td>
                <td><?php echo $item['latitud']?></td>
                <td><?php echo $item['longitud']?></td>
                <td style="text-align: center"><input type="checkbox" disabled <?php echo ($item["activo"]== 1) ? "checked ":"";?>></td>
                <td style="text-align: center"><a href="sucursal.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
                <td style="text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>
<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>
</body>
</html>





