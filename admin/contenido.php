<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("includes/Consultas.php");
include_once("includes/funciones.php");

$con=new Consultas();

$id=0;
$pnd=null;

if($_POST["modo"]=="update") {
    modifica_contenido();
}

$id=1;
$contenido=$con->get_contenido();

//var_dump($contenido);
$contenido=$contenido[0];
$logo=$contenido['logo'];
$nosotros_text=$contenido['nosotros_text'];
$nosotros_img=$contenido['nosotros_img'];
$ventas_url_1=$contenido['ventas_url_1'];
$ventas_img_1=$contenido['ventas_img_1'];
$ventas_url_2=$contenido['ventas_url_2'];
$ventas_img_2=$contenido['ventas_img_2'];
$ventas_text=$contenido['ventas_text'];
$resp_social_text=$contenido['resp_social_text'];
$resp_social_img=$contenido['resp_social_img'];
$video_url=$contenido['video_url'];
$tercerizacion_text=$contenido['tercerizacion_text'];
$tercerizacion_img=$contenido['tercerizacion_img'];
$destino_correo=$contenido['destino_correo'];




?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Contenido General</h3>
        <?php }else{?>
            <h3>Modificacion de Contenido</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=13 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="contenido.php" method="post" enctype="multipart/form-data">
            <?php /*if($id>0){ ?>
                <div class="form-group">
                    <label for="id">Id</label>
                    <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
                </div>
            <?php } */?>


            <div class="panel panel-info">
                <div class="panel-heading">Logo Santa Clara</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="logo">Imagen</label>
                                <input type="file" id="logo" name="logo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $logo?>" />
                        </div>
                    </div>
                </div>
            </div>


            <div class="panel panel-info">
                <div class="panel-heading">Contenido Seccion "Nosotros"</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="nosotros_text">Descripcion</label>
                        <textarea class="form-control" rows="3" id="nosotros_text" name="nosotros_text" ><?php echo ($id>0)? $nosotros_text:"" ?></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nosotros_img">Imagen</label>
                                <input type="file" id="nosotros_img" name="nosotros_img">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $nosotros_img?>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Contenido Seccion "Ventas"</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="ventas_url_1">Link 1</label>
                        <input type="text" class="form-control" id="ventas_url_1" name="ventas_url_1" placeholder="URL Link 1" value="<?php echo ($id>0)? $ventas_url_1:"" ?>" >
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ventas_img_1">Imagen Link 1</label>
                                <input type="file" id="ventas_img_1" name="ventas_img_1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $ventas_img_1?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ventas_url_1">Link 2</label>
                        <input type="text" class="form-control" id="ventas_url_2" name="ventas_url_2" placeholder="URL Link 2" value="<?php echo ($id>0)? $ventas_url_2:"" ?>" >
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="ventas_img_2">Imagen Link 2</label>
                                <input type="file" id="ventas_img_2" name="ventas_img_2">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $ventas_img_2?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="ventas_text">Descripcion</label>
                        <textarea class="form-control" rows="3" id="ventas_text" name="ventas_text" ><?php echo ($id>0)? $ventas_text:"" ?></textarea>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Contenido Seccion "Responsabilidad Social"</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="resp_social_text">Descripcion</label>
                        <textarea class="form-control" rows="3" id="resp_social_text" name="resp_social_text" ><?php echo ($id>0)? $resp_social_text:"" ?></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="resp_social_img">Imagen</label>
                                <input type="file" id="resp_social_img" name="resp_social_img">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $resp_social_img?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="video_url">Video URL</label>
                        <input type="text" class="form-control" id="video_url" name="video_url" placeholder="URL de Video" value="<?php echo ($id>0)? $video_url:"" ?>" >
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Contenido Seccion "Tercerizacion"</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="tercerizacion_text">Descripcion</label>
                        <textarea class="form-control" rows="3" id="tercerizacion_text" name="tercerizacion_text" ><?php echo $tercerizacion_text ?></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tercerizacion_img">Imagen</label>
                                <input type="file" id="tercerizacion_img" name="tercerizacion_img">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <img style="width: 180px" src="../img/contenido/<?php echo $tercerizacion_img?>" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-info">
                <div class="panel-heading">Destinatario de Correo</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="video_url">Direccion</label>
                        <input type="text" class="form-control" id="destino_correo" name="destino_correo" placeholder="Direccion" value="<?php echo ($id>0)? $destino_correo:"" ?>" >
                    </div>
                </div>
            </div>


            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="update">
        </form>
    </div>
</div>

</body>
</html>