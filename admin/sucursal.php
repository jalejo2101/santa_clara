<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("includes/Consultas.php");


$id=0;
$banner=null;
if($_GET["id"]!=null) {
    $id = $_GET["id"];
    $suc = $con->get_sucursal($id);
    $sucursal = $suc['sucursal'];
    $latitud = $suc['latitud'];
    $longitud = $suc['longitud'];
    $activo = ($suc['activo'] != null) ? 1 : 0;
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>


<body>
<div class="row">
    <div class="col-xs-8 col-xs-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Sucursales</h3>
        <?php }else{?>
            <h3>Modificacion de Sucursales</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=14?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">
        <form role="form" action="sucursales.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>
            <div class="form-group">
                <label for="sucursal">Sucursal</label>
                <input type="text" class="form-control" id="sucursal" name="sucursal" placeholder="Descripcion" value="<?php echo ($id>0)? $sucursal:"" ?>" >
            </div>
            <div class="form-group">
                <label for="latitud">Latitud</label>
                <input type="text" class="form-control" id="latitud" name="latitud" placeholder="00.00000" value="<?php echo ($id>0)? $latitud:"" ?>" >
            </div>
            <div class="form-group">
                <label for="longitud">Longitud</label>
                <input type="text" class="form-control" id="longitud" name="longitud" placeholder="00.00000" value="<?php echo ($id>0)? $longitud:"" ?>" >
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $suc["activo"]==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>

</body>
</html>