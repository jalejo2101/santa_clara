<?php include_once("includes/Consultas.php");?>
<?php include_once("includes/funciones.php");?>
<?php

if($_POST["modo"]=="new"){
    agrega_panaderia();
}else if($_POST["modo"]=="update"){
    modifica_panaderia();
}if($_POST["modo"]=="delete"){
    elimina_panaderia();
}

$con=new Consultas();
$lst=$con->get_panaderias();
?>

<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
    <script>
        function borrar(id)
        {
            document.fr.id.value=id;
            if(confirm("Esta seguro que desea eliminar este Banner?")){
                document.fr.submit();
            }
        }
    </script>
</head>
<body>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <h3>Listado de Panaderias</h3>
    </div>
    <div class="col-xs-2" style="padding-top:15px">
        <button type="button" class="btn btn-primary" style="width: 100%" onclick="window.open('panaderia.php','_self','')">Agregar</button>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=11 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <table class="table table-hover">
            <thead>
            <tr style="background: #9acfea">
                <td style="width: 5%">Id</td>
                <td style="width: 10%">Titulo</td>
                <td>Descripcion</td>
                <td style="width: 15%; text-align: center">Imagen</td>
                <td style="width: 10%; text-align: center">Tipo</td>
                <td style="width: 7%; text-align: center">Activo</td>
                <td style="width: 5%; text-align: center">M</td>
                <td style="width: 5%; text-align: center">E</td>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($lst as $item){ ?>
            <tr>
                <td><?php echo $item['id']?></td>
                <td><?php echo $item['titulo']?></td>
                <td><?php echo $item['text']?></td>
                <td style="; text-align: center"><img src="../img/panaderias/<?php echo $item['img']?>" style="width: 120px"></td>
                <td style="; text-align: center"><?php echo $item['tipo'] ?></td>
                <td style="; text-align: center"><input type="checkbox" disabled <?php echo ($item["activo"]== 1) ? "checked ":"";?>></td>
                <td style="; text-align: center"><a href="panaderia.php?id=<?php echo $item["id"]?>"><img src="img/edit_icon.png"></a></td>
                <td style="; text-align: center"><a href="javascript:borrar('<?php echo $item["id"]?>')"><img src="img/delete_icon.png"></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<form name="fr" method="post" action="">
    <input type="hidden" name="id">
    <input type="hidden" name="modo" value="delete">
</form>

</body>
</html>





