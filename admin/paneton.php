<?php
/**
 * Created by PhpStorm.
 * User: JuanJosé
 * Date: 07/08/14
 * Time: 09:17 AM
 */
include_once("includes/Consultas.php");
$con=new Consultas();

$id=0;
$pnd=null;
if($_GET["id"]!=null){
    $id=$_GET["id"];
    $pnd=$con->get_paneton($id);
    $titulo=$pnd['titulo'];
    $text=$pnd['text'];
    $img=$pnd['img'];
    $tipo=$pnd['tipo'];
    $activo=($pnd['activo']!=null)?1:0;
}?>
<!DOCTYPE html>
<html>
<head>
    <?php include("head.php");?>
    <title></title>
</head>
<body>
<div class="row">
    <div class="col-md-7 col-md-offset-3">
        <?php if($id==0){ ?>
            <h3>Insercion de Panetones</h3>
        <?php }else{?>
            <h3>Modificacion de Panetones</h3>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-2 col-xs-offset-1">
        <?php $op=12 ?>
        <?php include_once("menu.php")?>
    </div>
    <div class="col-xs-8">

        <form role="form" action="panetones.php" method="post" enctype="multipart/form-data">
            <?php if($id>0){ ?>
            <div class="form-group">
                <label for="id">Id</label>
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            </div>
            <?php } ?>

            <div class="form-group">
                <label for="tituloN">Titulo</label>
                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Titulo" value="<?php echo ($id>0)? $titulo:"" ?>" >
            </div>
            <div class="form-group">
                <label for="texto">Texto</label>
                <textarea class="form-control" rows="3" id="text" name="text" ><?php echo ($id>0)? $text:"" ?></textarea>
            </div>

            <div class="form-group">
                <label for="grupo">Tipo</label>
                <select class="form-control" name="tipo" id="tipo">
                    <option <?php echo ($tipo=="selecto")? "selected":"" ?> value="selecto">SELECTO</option>
                    <option <?php echo ($tipo=="tradicional")? "selected":"" ?> value="tradicional">TRADICIONAL</option>
                    <option <?php echo ($tipo=="gaspare")? "selected":"" ?> value="gaspare">GASPARE</option>
                </select>

            </div>
            <div class="form-group">
                <label for="file">Adjuntar imagen</label>
                <input type="file" id="file" name="file">
                <?php echo ($id>0)? "<help>".$news["imagen"]."</help>":""?>
            </div>
            <div class="checkbox">
                <label>
                    <input name="activo" type="checkbox" <?php echo ($id>0 && $pnd["activo"]==1)? "checked":""?> > Activo
                </label>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
            <input type="hidden" name="modo" value="<?php echo ($id==0)? "new":"update"?>">
        </form>
    </div>
</div>

</body>
</html>