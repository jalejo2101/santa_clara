<?php
session_start();
?>
<meta charset="utf-8">
<link rel="icon" type="image/png" href="../imgs/favicon.ico" />
<!--meta http-equiv="X-UA-Compatible" content="IE=edge"-->
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="css/reset.css" type="text/css" rel="stylesheet">
<link href="css/layout.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!--script src="js/nicEdit.js"></script-->
<script>
    bkLib.onDomLoaded(nicEditors.allTextAreas);
</script>
<script>
    (function($){
        $(window).load(function(){
            $("#text").mCustomScrollbar({
                theme:"dark"
            });
        });
    })(jQuery);
</script>



