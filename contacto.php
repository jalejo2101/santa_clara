    <section id="contacto" class="minAlto">
        <div class="sectionSpaces dashedLine container">
            <h2 class="center">Contactos</h2>
        </div>        
            <div class="formwrapper">
                <div class="triangulito"></div>
                <form class="container">
                    <input type="text" name="nombre" id="nombre" placeholder="Nombres y Apellidos">
                    <input type="text" name="phone" id="phone" placeholder="Teléfono">
                    <input type="text" name="mail" id="mail" placeholder="Email">
                    <textarea name="comentario" id="comentario" placeholder="Comentario"></textarea>
                    <a class="botonSend" id="botonSend"><span>enviar</span></a>
                    <div id="msg" style="width: 200px; height: 30px; background-color: transparent; margin: auto">

                    </div>
                </form>
                <footer> <h5>© SANTA CLARA PERÚ 2015 - Derechos Reservados </h5></footer>
            </div>
    </section>
    <script language="JavaScript">
        $("document").ready(function(){
            $("#botonSend").click(function(){
                $.ajax({
                    url: "send_mail.php",
                    method: "POST",
                    data:{nombre:$("#nombre").val(), phone:$("#phone").val(), mail:$("#mail").val(), comentario:$("#comentario").val(), destinatario:"<?php echo $cont['destinatario']?>" }
                })
                    .done(function( msg ) {
                        $("#msg").empty().append("<div id='enviado'>Mensaje enviado!</div>");
                        $("#nombre").val("");
                        $("#phone").val("");
                        $("#mail").val("");
                        $("#comentario").val("");
                    });
            });
        });
    </script>
