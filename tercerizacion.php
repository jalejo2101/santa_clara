    <section id="tercerizacion" class="minAlto">
        <div class="sectionSpaces dashedLine container">
           <div class="row">
               <div class="col l8 m7 s12">
                   <h2>Tercerización</h2>
                   <p>
                    <?php echo $cont['tercerizacion_text']?>
                   </p>
               </div>
               <div class="center col l4 m5 s12">
                   <img class="responsive-img" src="img/contenido/<?php echo $cont['tercerizacion_img']?>">
               </div>
           </div>
        </div>
    </section>